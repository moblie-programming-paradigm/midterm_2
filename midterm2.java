import java.util.Scanner;

public class midterm2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String S = sc.next();
        System.out.println(StrToInt(S));
}

    private static int StrToInt(String S) {
        int res = 0;
        for (int i = 0; i <= S.length() - 1; i++) {
            res = res * 10 + S.charAt(i) - '0';
        }
        return res;
    }
    }
